FROM debian:wheezy

MAINTAINER Baptiste Donaux <bdonaux@wanadev.fr>

RUN apt-get update \
    && apt-get install -y \
        nginx

WORKDIR /home/docker

COPY nginx.conf /etc/nginx/nginx.conf

COPY service_start.sh /home/docker/script/service_start.sh
RUN chmod 744 /home/docker/script/service_start.sh

ENTRYPOINT /home/docker/script/service_start.sh

